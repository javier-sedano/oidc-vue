#!/bin/bash

docker exec -ti oidc-vue-keycloak /opt/keycloak/bin/kc.sh export --file /tmp/broker.json --realm BROKER
docker cp oidc-vue-keycloak:/tmp/broker.json .

docker exec -ti oidc-vue-keycloak /opt/keycloak/bin/kc.sh export --file /tmp/brokerhint.json --realm BROKERHINT
docker cp oidc-vue-keycloak:/tmp/brokerhint.json .

docker exec -ti oidc-vue-keycloak /opt/keycloak/bin/kc.sh export --file /tmp/j.json --realm J
docker cp oidc-vue-keycloak:/tmp/j.json .

docker exec -ti oidc-vue-keycloak /opt/keycloak/bin/kc.sh export --file /tmp/k.json --realm K
docker cp oidc-vue-keycloak:/tmp/k.json .

docker exec -ti oidc-vue-keycloak /opt/keycloak/bin/kc.sh export --file /tmp/l.json --realm L
docker cp oidc-vue-keycloak:/tmp/l.json .
