#!/bin/bash

docker exec -ti -u root oidc-vue-keycloak rm -f /opt/keycloak/providers/keycloak-home-idp-discovery.jar build
docker cp keycloak-home-idp-discovery.jar oidc-vue-keycloak:/opt/keycloak/providers/
docker exec -ti -u keycloak oidc-vue-keycloak /opt/keycloak/bin/kc.sh build

docker exec -ti -u root oidc-vue-keycloak rm -f /tmp/broker-import.json
docker cp broker.json oidc-vue-keycloak:/tmp/broker-import.json
docker exec -ti -u keycloak oidc-vue-keycloak /opt/keycloak/bin/kc.sh import --file /tmp/broker-import.json

docker exec -ti -u root oidc-vue-keycloak rm -f /tmp/brokerhint-import.json
docker cp brokerhint.json oidc-vue-keycloak:/tmp/brokerhint-import.json
docker exec -ti -u keycloak oidc-vue-keycloak /opt/keycloak/bin/kc.sh import --file /tmp/brokerhint-import.json

docker exec -ti -u root oidc-vue-keycloak rm -f /tmp/j-import.json
docker cp j.json oidc-vue-keycloak:/tmp/j-import.json
docker exec -ti -u keycloak oidc-vue-keycloak /opt/keycloak/bin/kc.sh import --file /tmp/j-import.json

docker exec -ti -u root oidc-vue-keycloak rm -f /tmp/k-import.json
docker cp k.json oidc-vue-keycloak:/tmp/k-import.json
docker exec -ti -u keycloak oidc-vue-keycloak /opt/keycloak/bin/kc.sh import --file /tmp/k-import.json

docker exec -ti -u root oidc-vue-keycloak rm -f /tmp/l-import.json
docker cp l.json oidc-vue-keycloak:/tmp/l-import.json
docker exec -ti -u keycloak oidc-vue-keycloak /opt/keycloak/bin/kc.sh import --file /tmp/l-import.json

docker container stop oidc-vue-keycloak
docker container start oidc-vue-keycloak
