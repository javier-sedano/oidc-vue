import axios from 'axios';
import { getRequestHeaders } from '@/services/OidcService';
import { ConfigurationService } from '../ConfigurationService';

interface HelloResponse {
  msg: string;
}

export class HelloService {
  hello = async (): Promise<string> => (
    await axios.get<HelloResponse>(ConfigurationService.backend, {
      headers: await getRequestHeaders(),
    })
  ).data.msg;
}

export const helloService: HelloService = new HelloService();
