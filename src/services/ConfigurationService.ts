const IDP = 'IDP';
const CLIENT_ID = 'CLIENT_ID';
const CLIENT_SECRET = 'CLIENT_SECRET';
const SCOPE = 'SCOPE';
const BASEURL = 'BASEURL';
const BACKEND = 'BACKEND';
const EXTRA_QUERY_PARAMS_JSON = 'EXTRA_QUERY_PARAMS_JSON';

export class ConfigurationService {
  static get idp(): string {
    return localStorage.getItem(IDP) !== null ? localStorage.getItem(IDP) : process.env.VUE_APP_IDP;
  }

  static set idp(value: string) {
    console.log(`Setting idp ${value}`);
    localStorage.setItem(IDP, value);
  }

  static resetIdp() {
    this.idp = process.env.VUE_APP_IDP;
  }

  static get clientId(): string {
    return localStorage.getItem(CLIENT_ID) !== null
      ? localStorage.getItem(CLIENT_ID)
      : process.env.VUE_APP_CLIENT_ID;
  }

  static set clientId(value: string) {
    localStorage.setItem(CLIENT_ID, value);
  }

  static resetClientId() {
    this.clientId = process.env.VUE_APP_CLIENT_ID;
  }

  static get clientSecret(): string {
    return localStorage.getItem(CLIENT_SECRET) !== null
      ? localStorage.getItem(CLIENT_SECRET)
      : process.env.VUE_APP_CLIENT_SECRET;
  }

  static set clientSecret(value: string) {
    localStorage.setItem(CLIENT_SECRET, value);
  }

  static resetClientSecret() {
    this.clientSecret = process.env.VUE_APP_CLIENT_SECRET;
  }

  static get scope(): string {
    return localStorage.getItem(SCOPE) !== null
      ? localStorage.getItem(SCOPE)
      : process.env.VUE_APP_SCOPE;
  }

  static set scope(value: string) {
    localStorage.setItem(SCOPE, value);
  }

  static resetScope() {
    this.scope = process.env.VUE_APP_SCOPE;
  }

  static get baseUrl(): string {
    return localStorage.getItem(BASEURL) !== null
      ? localStorage.getItem(BASEURL)
      : process.env.VUE_APP_BASEURL;
  }

  static set baseUrl(value: string) {
    localStorage.setItem(BASEURL, value);
  }

  static resetBaseUrl() {
    this.baseUrl = process.env.VUE_APP_BASEURL;
  }

  static get backend(): string {
    return localStorage.getItem(BACKEND) !== null
      ? localStorage.getItem(BACKEND)
      : process.env.VUE_APP_BACKEND;
  }

  static set backend(value: string) {
    localStorage.setItem(BACKEND, value);
  }

  static resetBackend() {
    this.backend = process.env.VUE_APP_BACKEND;
  }

  static get extraQueryParamsJson(): string {
    return localStorage.getItem(EXTRA_QUERY_PARAMS_JSON) !== null
      ? localStorage.getItem(EXTRA_QUERY_PARAMS_JSON)
      : process.env.VUE_APP_EXTRA_QUERY_PARAMS_JSON;
  }

  static set extraQueryParamsJson(value: string) {
    localStorage.setItem(EXTRA_QUERY_PARAMS_JSON, value);
  }

  static resetExtraQueryParamsJson() {
    this.extraQueryParamsJson = process.env.VUE_APP_EXTRA_QUERY_PARAMS_JSON;
  }

  static reset() {
    this.resetIdp();
    this.resetClientId();
    this.resetClientSecret();
    this.resetScope();
    this.resetBaseUrl();
    this.resetBackend();
    this.resetExtraQueryParamsJson();
  }
}

export const configurationService: ConfigurationService = new ConfigurationService();
