import {
  UserManagerSettings,
  Log,
  User,
  UserLoadedCallback,
  SilentRenewErrorCallback,
  UserManager,
  ExtraSigninRequestArgs,
} from 'oidc-client-ts';
import jwtDecode from 'jwt-decode';
import axios, { AxiosRequestHeaders } from 'axios';
import { ConfigurationService } from './ConfigurationService';

export const PATH_FOR_AUTHWITHREDIRECT = 'authWithRedirect';
export const PATH_FOR_AUTHWITHPOPUP = 'authWithPopup';
export const BROKER_KEYWORD = 'BROKER';

function domainOf(email?: string): string {
  if (email === undefined) {
    return '';
  }
  return email.substring(email.lastIndexOf('@') + 1);
}

function buildExtraSigninQueryParams(email: null | string): ExtraSigninRequestArgs {
  if (email && email.includes('@')) {
    return {
      extraQueryParams: {
        kc_idp_hint: domainOf(email),
        login_hint: email,
      },
    };
  }
  return {};
}

export class OidcService {
  private userManager: UserManager;

  constructor() {
    Log.setLogger(console);
    Log.setLevel(Log.DEBUG);
    let extraQueryParams: Record<string, string | number | boolean> = {};
    try {
      extraQueryParams = JSON.parse(ConfigurationService.extraQueryParamsJson);
    } catch (e) {
      console.log(`extraQueryParamsJson: ${ConfigurationService.extraQueryParamsJson}`);
      console.error(e);
    }
    const userManagerSettings: UserManagerSettings = {
      authority: ConfigurationService.idp,
      client_id: ConfigurationService.clientId,
      scope: ConfigurationService.scope,
      redirect_uri: `${ConfigurationService.baseUrl}${PATH_FOR_AUTHWITHREDIRECT}`,
      post_logout_redirect_uri: ConfigurationService.baseUrl,
      extraQueryParams,
    };
    if (ConfigurationService.clientSecret !== '') {
      userManagerSettings.client_secret = ConfigurationService.clientSecret;
    }
    this.userManager = new UserManager(userManagerSettings);
  }

  public async loginAuthorizationCodeFlowWithPkceRedirect(email: null | string): Promise<void> {
    await this.userManager.signinRedirect({
      redirect_uri: `${ConfigurationService.baseUrl}${PATH_FOR_AUTHWITHREDIRECT}`,
      ...buildExtraSigninQueryParams(email),
    });
  }

  public async loginAuthorizationCodeFlowWithPkceRedirectCallback(): Promise<void> {
    await this.userManager.signinRedirectCallback();
  }

  public async loginAuthorizationCodeFlowWithPkcePopup(email: null | string): Promise<void> {
    await this.userManager.signinPopup({
      redirect_uri: `${ConfigurationService.baseUrl}${PATH_FOR_AUTHWITHPOPUP}`,
      ...buildExtraSigninQueryParams(email),
    });
  }

  public async loginAuthorizationCodeFlowWithPkcePopupCallback(): Promise<void> {
    await this.userManager.signinPopupCallback();
  }

  public async loginResourceOwnerPasswordFlow(username: string, password: string): Promise<void> {
    await this.userManager.signinResourceOwnerCredentials({
      username,
      password,
    });
  }

  public async logout(): Promise<void> {
    await this.userManager.signoutRedirect();
  }

  public async getUser(): Promise<User | null> {
    return this.userManager.getUser();
  }

  public addUserLoadedCallback(userLoadedCallback: UserLoadedCallback): void {
    this.userManager.events.addUserLoaded(userLoadedCallback);
  }

  public addSilentRenewErrorCallback(silentRenewErrorCallback: SilentRenewErrorCallback): void {
    this.userManager.events.addSilentRenewError(silentRenewErrorCallback);
  }
}

export const oidcService: OidcService = new OidcService();

export function jwt2String(jwt: string | undefined): string {
  try {
    if (jwt === undefined) {
      return '';
    }
    const header = jwtDecode(jwt, {
      header: true,
    });
    const headerJson = JSON.stringify(header, null, 2);
    const payload = jwtDecode(jwt);
    const payloadJson = JSON.stringify(payload, null, 2);
    return `${headerJson}\n${payloadJson}`;
  } catch (e) {
    return 'Not a JWT token?';
  }
}

export async function getRequestHeaders(): Promise<AxiosRequestHeaders> {
  const user: User | null = await oidcService.getUser();
  if (user?.access_token !== null) {
    return {
      Authorization: `Bearer ${user?.access_token}`,
    };
  }
  return {};
}

export interface ExternalIdpToken {
  // eslint-disable-next-line
  id_token?: string,
  // eslint-disable-next-line
  access_token?: string,
  // eslint-disable-next-line
  refresh_token?: string,
}

export async function getExternalIdpToken(user: User | null): Promise<ExternalIdpToken | null> {
  if (!ConfigurationService.idp.includes(BROKER_KEYWORD)) {
    return null;
  }
  if (user === null) {
    return null;
  }
  console.log('IdentityBroker detected, trying to obtain the ExternalIdp tokens');
  const domain = domainOf(user.profile.email);
  const externalIdpTokenUrl = `${ConfigurationService.idp}broker/${domain}/token`;
  try {
    const externalIdpToken = (await axios.get<ExternalIdpToken>(externalIdpTokenUrl, {
      headers: await getRequestHeaders(),
    })).data;
    return externalIdpToken;
  } catch (error) {
    console.log('Unable to retrieve ExternalIdp tokens, proceeding anyway');
    console.log(error);
    return null;
  }
}
