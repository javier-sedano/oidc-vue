import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import AuthWithRedirectView from '../views/AuthWithRedirectView.vue';
import AuthWithPopupView from '../views/AuthWithPopupView.vue';
import { PATH_FOR_AUTHWITHPOPUP, PATH_FOR_AUTHWITHREDIRECT } from '../services/OidcService';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
  {
    path: `/${PATH_FOR_AUTHWITHREDIRECT}`,
    name: 'AuthWithRedirect',
    component: AuthWithRedirectView,
  },
  {
    path: `/${PATH_FOR_AUTHWITHPOPUP}`,
    name: 'AuthWithPopup',
    component: AuthWithPopupView,
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
  },
];

export const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});
