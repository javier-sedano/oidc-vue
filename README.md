Simple oidc client using Keycloak as IdP (or others, theoretically).

## Requirements

It is suggested to open the project in an containerized VSCode environment:

* VSCode with extension "Remote - Containers"
* Docker (Rancher Desktop is tested)
  * Open VSCode, Command palete, Clone in named container volume, type the URL of the git repo and follow instructions. It will clone and open directly in container.

If no containerized, the following environment is suggested:

* VSCode with "Prettier ESLint"
* Node 14
* yarn
* Docker (Rancher Desktop for Mac has been tested) for Keycloack
* Use the existing scripts and realm export files as inspiration, but do not expect all of the to work out of the box.

## Scripts

All of the common tasks are kept as package.json scripts:

* Project setup: `yarn install`
* Build and serve for development: `yarn serve`
* Open the browser in the right port with the right name: `yarn browse`
* Build for production: `yarn build`
* Unit tests: `yarn test:unit` (no tests, it is a toy project)
* Configurable in `.env*`, but it matches the instructions bellow
* Export/Import the KeyCloak realms with `yarn kc:export` and `yarn kc:import`

## Deployment in "production"

Automatically deployed to ["production"](https://jsedano.duckdns.org/oidc-vue/). You can configure the OIDC parameters there, if you want to test your own IdP.

I say "production", quoted, because... well... it is a toy application, not useful for anything... But well, there it is.

# IdP
You can use several IdP for this test: local Keycloak (default config), cloud Keycloak at Cloud AIM, cloud Auth0 or you own IdP.

By default, it is configured in local development to use the local Keycloak and in ["production"](https://jsedano.duckdns.org/oidc-vue/) to use cloud Keycloak at Cloud IAM (see bellow), but you can configure it to use other IdP. See bellow.
  * SECURITY WARNING: allowing these parameters to be configurable in a real application is a security risk. DO NOT DO IT unless you know what you are doing.

## Local Keycloak

This is de default setup for local development. The Keycloak container is already running.

You can import the default configuration for the Realm J using `yarn kc:import`; browse using http://localhost:8080/ and login with admin/admin.

* For local development:
  * Run in local with `yarn serve`
  * Browse http://localhost:3000/
* For ["production"](https://jsedano.duckdns.org/oidc-vue/):
  * Browse https://jsedano.duckdns.org/oidc-vue/

The default user is aaa/a

## Cloud Keycloak at Cloud IAM

This is the default setup for the ["production"](https://jsedano.duckdns.org/oidc-vue/) environment.

* Create an account in https://cloud-iam.com/ (free tier available)
* Configure following the wizard (especially, create a deployment, which in turn creates a Realm)
  * Self hint: my deployment is managed at https://lemur-12.cloud-iam.com/auth/admin/j/console/#/realms/j , user admin, password stored in my password manager.
* Create a user and set its credentials
  * Optionally (needed for the user to see his data): Client Scopes, Roles, Mappers, client roles: verify that "Add to access token is enabled".
* For local development:
  * Run in local with `yarn serve`
  * In Cloud IAM, create a Client `oidc-vue`:
    * Use Base Url `http://localhost:3000/`
      * Optionally, set Base URL to `/` and remove the trailing `/` from Root URL (just some easier links in Keycloak)
  * Browse `http://localhost:3000/`
    * Configure the OIDC client with:
      * IdP: `https://YOURDOMAIN.cloud-iam.com/auth/realms/YOURREALM/` (you will find this data by inspecting the URL of the Cloud IAM Keycloak deployment or inspecting Realm Settings \ General \ Endpoints \ OpenID Enpoint Configuration (not the best UX, yeah))
      * Client ID: `oidc-vue`
      * App base URL: `https://clientapp.com:3000/`
      * Backend endpoint: `https://clientapp.com:3000/backend/hello.json`
* For ["production"](https://jsedano.duckdns.org/oidc-vue/):
  * In Cloud IAM, create a Client `oidc-vue-jsedano`
    * Use Base Url `https://jsedano.duckdns.org/oidc-vue/`
      * Optionally, set Base URL to `/` (just some easier links in Keycloak)
  * Browse https://jsedano.duckdns.org/oidc-vue/
    * Configure the OIDC client with:
      * IdP: `https://YOURDOMAIN.cloud-iam.com/auth/realms/YOURREALM/` (you will find this data by inspecting the URL of the Cloud IAM Keycloak deployment or inspecting Realm Settings \ General \ Endpoints \ OpenID Enpoint Cnfiguration (not the best UX, yeah))
      * Client ID: `oidc-vue-jsedano`
      * App base URL: `https://jsedano.duckdns.org/oidc-vue/`
      * Backend endpoint: `https://jsedano.duckdns.org/oidc-vue/backend/hello.json`

## Auth0

* Create an account in https://www.auth0.com (free tier available)
* Configure it following the wizards (especially, branding, which is mandatory)
* Optionally configure the Authentication \ Database \ Username-Password-Authentication password policy
* Optionally create a new user
* For local development
  * Pre requirement: Auth0 only works on https and not in localhost, so:
    * Add `127.0.0.1 clientapp.com` to your `/etc/hosts`
    * Activate https in `vue.config.js`
    * Run in local with `yarn serve`
  * In Auth0, create a new Application of type SPA
    * Use `https://clientapp.com:3000/` as application URL (in several fields) and `https://clientapp.com:3000/auth` as Allowed Callback URLs
  * Browse https://clientapp.com:3000/
    * Configure the OIDC client with:
      * IdP: `https://YOURDOMAIN.eu.auth0.com/`
      * Client ID: get it in Auth0 application settings
      * App base URL: `https://clientapp.com:3000/`
      * Backend endpoint: `https://clientapp.com:3000/backend/hello.json`
  * Auth0 does not publish its logout endpoint, so logout through https://jsedano.eu.auth0.com/logout
* For ["production"](https://jsedano.duckdns.org/oidc-vue/):
  * In Auth0, create a new Application
    * Use `https://jsedano.duckdns.org/oidc-vue/` as application URL (in several fields) and `https://jsedano.duckdns.org/oidc-vue/auth` as Allowed Callback URLs
  * Browse https://jsedano.duckdns.org/oidc-vue/
    * Configure the OIDC client with:
      * IdP: `https://YOURDOMAIN.eu.auth0.com/`
      * Client ID: get it in Auth0 application settings
      * App base URL: `https://jsedano.duckdns.org/oidc-vue/`
      * Backend endpoint: `https://jsedano.duckdns.org/oidc-vue/backend/hello.json`
  * Auth0 does not publish its logout endpoint, so logout through https://jsedano.eu.auth0.com/logout
* "Account" link will not work
## Your own IdP

To use your own Identity Provider, you need to configure it to accept requests from the application at `http://localhost:3000/` or `https://jsedano.duckdns.org/oidc-vue/` (depending on which one you are configuring).

This client asumes a Client Type: Public (see https://datatracker.ietf.org/doc/html/rfc6749#section-2.1 or https://auth0.com/docs/get-started/applications/confidential-and-public-applications), because the credentials cannot be kept secure in the client (it is an SPA).

This client assumes that the IdP is using OpenID Connect Discovery (https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderConfigurationRequest). Especifically, it will start by downloading the file `/.well-known/openid-configuration` from the configured IdP URL.

Then, you need to browse `http://localhost:3000/` or `https://jsedano.duckdns.org/oidc-vue/` respectively and fill:

* IdP: the base URL for the IdP. `BASEURL/.well-known/openid-configuration` must provide the OIDC configuration of the IdP.
* Client ID: the identifier of the Client Application. How to define or obtain it depends on the IdP implementation.
* App base URL: the URL where this application is deployed (usually `http://localhost:3000/` or `https://jsedano.duckdns.org/oidc-vue/` if you are using the local development or the ["production"](https://jsedano.duckdns.org/oidc-vue/) environment, respectively).
* Backend endpoint: the URL of the backend emulated call (see below).

# API call emulation

The button `API call` emulates a call to an API using GET. It expects a JSON with a property `msg` and dumps its content in an alert.

Once logged, it attaches the access_token to the request... letting you verify that the access token is correct.

By default, the current client includes a fake backend with a hard-coded HelloWorld response, but you can provide you own backend URL for testing, if you want; if it does not return a JSON object with a `msg` property, it will fail... but it does not matter, you can still inspect the request, the response,...

# Tokens

The ID token is decoded as JWT (it is expected to be a JWT).

The access token and the refresh token may or may not be a JWT... but they usually are. So it just tries to decode it as JWT... if they are not JWT, no problem, it will not fail, it just will not be decoded.

# Identity broker

In this mode of operation, KeyCloak operates as a broker which, depending on the email domain, forwards the request to another IdP (in our example, all of the other IdP are actually Realms in the same KeyCloak, but in a general case, they would be different servers most probably in a different organization).

This is an overview of the concept: Keycloak documentation: https://www.keycloak.org/docs/latest/server_admin/index.html#_identity_broker_overview

There are two examples of identity broker: one of them using only capabilities built in into KeyCloak, and another one using an custom authenticator. Both of them redirect to the appropriate delegate IdP depending on the domain of the email of the user, but using different mechanisms. The third option would be... not to do this automatic redirection... but then, the user needs to click in the appropriate delegated IdP. If they are social IdP (such as Google, etc), that's not a problem; but for custom IdP (for example, from third-party partners), this might me a privacy problem. We have not demonstrated this third option, because this is more or less the straightforward solution from the documentation of KeyCloak.

In both cases, the default configuration included in this repo uses the Base theme for the broker, to make clearly visible when we are in the broker and when in the delegated IdPs.

In both cases, the login process will try to download and decode the access token of the delegated IdP (see https://www.keycloak.org/docs/latest/server_admin/#retrieving-external-idp-tokens ). The delegated IdP for J (see below) allows this, the others do not. WARNING: If the IdBroker already contains old users from previous tests, they may not have the proper roles; the fastest way to solve it is just deleting the users (they will be reimported in the next login).

This configuration is not deployed to "production" because it is a bit more advanced than the basic oidc-vue; additionally, the solution with the Custom Authenticator needs deploying additinal plugins into KeyCloak, something that obvioulsy cloud-based IAMs are not allowing. So if you need to learn aboout it, you must clone the repo and do it yourself in your local development environment. Don't worry, it is mostly automated, except for the selecion of the IdP.

## Identity broker with built-in capabilities

Pros: Only capabitlities built-in into KeyCloak

Cons: In order to allow KeyCloak to do the automatic redirection depending on the domain of the email of the user, the client application must request the email itself and send kc_idp_hint accordingly

* The realm BROKERHINT acts as identity broker.
  * Configure oidc-vue to use `http://localhost:8080/realms/BROKERHINT/` as IdP (remember to Save after changing it).
  * oidc-vue automatically shows the field to write the email when the URL of the IdP contains the word `HINT`.
* The following realms act as delegated IdPs:
  * J: OIDC type associated with emails @j.com
    * Contains a user `aaa` with email `a@j.com` and password `a`.
  * K: OIDC type associated with emails @k.com
    * Contains a user `bbb` with email `b@k.com` and password `b`.
  * L: SAML type associated with emails @l.com
    * Contains a user `ccc` with email `c@l.com` and password `c`.
    * Logout in the application triggers logout in the id-broker, but not in the SAML delegated IdP (it fails with an error in the logs that looks like a bug, as of 12/05/2024, Keycloak 24.0.4).

## Identity broker with custom Authentication Provider

Pros: The client application does not need to know that we are doing this, does not need to send kc_idp_hint.

Cons: In order to do the automatic redirection depending on the email, a custom Authentication Provider must be added to KeyCloak. In this repository, it is already added, but read [Home IdP discovery](https://github.com/sventorben/keycloak-home-idp-discovery) for more information. In particular, the association of email domains to delegated IdPs is a bit tricky, because there is no GUI to do it. It can be done through the CLI with somethign similar to `/opt/keycloak/bin/kcadm.sh update identity-provider/instances/j.com --no-config --server http://localhost:8080 --realm master --user admin --password admin -r BROKER -s 'config."home.idp.discovery.domains"="j.com"'`; Even worse: for the delegated IdP of type SAML, this value is lost when other configurations are changed, so you will need to add that mapping again.

* The realm BROKER acts as identity broker.
  * Configure oidc-vue to use `http://localhost:8080/realms/BROKER/` as IdP (remember to Save after changing it).
* The following realms act as delegated IdPs:
  * J: OIDC type associated with emails @j.com
    * Contains a user `aaa` with email `a@j.com` and password `a`.
  * K: OIDC type associated with emails @k.com
    * Contains a user `bbb` with email `b@k.com` and password `b`.
  * L: SAML type associated with emails @l.com
    * Contains a user `ccc` with email `c@l.com` and password `c`.
    * Logout in the application triggers logout in the id-broker, but not in the SAML delegated IdP (it fails with an error in the logs that looks like a bug, as of 12/05/2024, Keycloak 24.0.4).

# References
* OIDC TS library: https://github.com/authts/oidc-client-ts
* OIDS JS library (doc for oidc-client-ts): https://github.com/IdentityModel/oidc-client-js/wiki
* Decoder for JWT: https://github.com/auth0/jwt-decode
* Inspiration: https://damienbod.com/2019/01/29/securing-a-vue-js-app-using-openid-connect-code-flow-with-pkce-and-identityserver4/
* Keycloak: https://www.keycloak.org/
* Auth0: https://www.auth0.com
* Direct access grant: https://www.keycloak.org/docs/latest/server_admin/index.html#_oidc-auth-flows-direct
* Resource Owner Password Credential Grant: https://www.rfc-editor.org/rfc/rfc6749#section-4.3
