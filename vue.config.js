const path = require('path');

const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    allowedHosts: 'all',
//    https: true,
  },
  publicPath: process.env.NODE_ENV === 'production' ? '/oidc-vue/' : '/',
});
