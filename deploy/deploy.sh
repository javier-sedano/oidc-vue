#!/bin/bash
pushd `dirname "$0"`

rm -rf /var/www/html_oidc-vue/*
install -d /var/www/html_oidc-vue/
cp -r dist/* /var/www/html_oidc-vue/

popd
